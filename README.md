In this literature review I explore the Overhauser hyperpolarization technique for contrast enhanced MRI imaging and its potential for diagnostic application.

<object data="https://gitlab.com/thijsbs-portfoio/overhauser-enhanced-mri/raw/master/Overhauser%20Enhanced%20MRI%20literature%20review.pdf?inline=false" type="application/pdf" width="700px" height="700px">
    <embed src="https://gitlab.com/thijsbs-portfoio/overhauser-enhanced-mri/raw/master/Overhauser%20Enhanced%20MRI%20literature%20review.pdf?inline=false">
        <p>Please download the PDF to view it: <a href="https://gitlab.com/thijsbs-portfoio/overhauser-enhanced-mri/raw/master/Overhauser%20Enhanced%20MRI%20literature%20review.pdf?inline=false">Download PDF</a>. Or preview it in gitlab</p>
    </embed>
</object>

